package rvink;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class DateReducer extends Reducer<LongWritable, IntWritable, LongWritable, IntWritable> {

private IntWritable _value = new IntWritable();
	
	@Override
	public void reduce(LongWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		int count = 0;
		for(IntWritable value : values) {
			count += value.get();
		}
		_value.set(count);
		context.write(key, _value);
	}
}

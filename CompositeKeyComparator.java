package rvink;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class CompositeKeyComparator extends WritableComparator {

	protected CompositeKeyComparator() {
		super(DateTimeKey.class,  true);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2) {

		DateTimeKey key1 = (DateTimeKey) w1;
		DateTimeKey key2 = (DateTimeKey) w2;

		int cmp = key1.getGroup().compareTo(key2.getGroup());
		if (cmp != 0) {
			return cmp;
		}

		return key1.getDatetime() == key2.getDatetime() ? 0 : (key1
				.getDatetime() < key2.getDatetime() ? -1 : 1);

	}
}

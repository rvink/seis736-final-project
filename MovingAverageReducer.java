package rvink;

import java.io.IOException;
import java.text.SimpleDateFormat;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MovingAverageReducer  extends Reducer<DateTimeKey, IntWritable, Text, DoubleWritable> {

	private CumulativeMovingAverage _avg = new CumulativeMovingAverage();	
	private DoubleWritable _value = new DoubleWritable();
	private Text _key = new Text();
	
	@Override
	public void reduce(DateTimeKey key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		for(IntWritable value : values) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yyyy");
			_key.set(simpleDateFormat.format(key.getDatetime()));
			_value.set(_avg.add(value.get()));
			context.write(_key, _value);
		}
	}
	
	class CumulativeMovingAverage {

		int counter = 0;
		
		double average = 0.0;
		
		public double add(double x) {
			return average += (x - average) / ++counter;
		}
	}
}

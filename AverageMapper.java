package rvink;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class AverageMapper extends Mapper<LongWritable, Text, DateTimeKey, IntWritable> {

	private DateTimeKey _key = new DateTimeKey();
	private IntWritable _value = new IntWritable();
	 
	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String line = value.toString();
		String[] elements = line.split(",");
		_key.set("1", Long.parseLong(elements[0]));
		_value.set(Integer.parseInt(elements[1]));
		context.write(_key, _value);
	}
}

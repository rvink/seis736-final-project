package rvink;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.WritableComparable;


public class DateTimeKey implements WritableComparable<DateTimeKey> {

	private String group = "";
	
	private long datetime = 0;
	
	public void set(String group, long datetime) {
		this.group = group;
		this.datetime = datetime;
	}
	
	public String getGroup() {
		return this.group;
	}

	public long getDatetime() {
		return this.datetime;
	}
	
	@Override
	public void write(DataOutput out) throws IOException {
		out.writeUTF(group);
		out.writeLong(this.datetime);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.group = in.readUTF();
		this.datetime = in.readLong();
	}

	@Override
	public int compareTo(DateTimeKey o) {
		if(this.group.compareTo(o.group) != 0) {
			return this.group.compareTo(o.group);
		} else if(this.datetime != o.datetime) {
			return this.datetime < o.datetime ? -1 : 1;
		}
		return 0;
	}
}

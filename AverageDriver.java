package rvink;

import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;

public class AverageDriver extends Configured implements Tool {
	
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		int exitCode = ToolRunner.run(conf, new AverageDriver(), args);
		System.exit(exitCode);
	}

	public int run(String[] args) throws Exception {
		if (args.length < 3) {
			System.out.printf("Usage: Driver <input dir> <output dir> <mapper> \n");
			System.exit(-1);
		}
		
		Configuration conf = super.getConf();
		conf.set("mapred.textoutputformat.separator",",");
		if(args.length == 4) {
			if(Float.parseFloat(args[3]) > 1) {
				System.out.printf("<alpha> must be less than 1\n");
				System.exit(-1);
			}
			if(Float.parseFloat(args[3]) < 0) {
				System.out.printf("<alpha> must be greater than 0\n");
				System.exit(-1);
			}
			
			conf.set("alpha",args[3]);
		}
		Job job = new Job(conf);
		if(args[2].equals("moving")) {
			job.setReducerClass(MovingAverageReducer.class);
		} else if(args[2].equals("exp")) {
			job.setReducerClass(ExponentialAverageReducer.class);
		}
		job.setMapperClass(AverageMapper.class);
		job.setJarByClass(AverageDriver.class);
		job.setOutputKeyClass(DateTimeKey.class);
		job.setOutputValueClass(IntWritable.class);
		job.setSortComparatorClass(CompositeKeyComparator.class);
		job.setNumReduceTasks(1);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		boolean success = job.waitForCompletion(true);
		return success ? 0 : 1;
	}

}

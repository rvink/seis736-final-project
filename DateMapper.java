package rvink;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import rvink.DateDriver.Accidents;

public class DateMapper extends Mapper<LongWritable, Text, LongWritable, IntWritable> {

	private LongWritable _key = new LongWritable();
	private IntWritable _value = new IntWritable();
	
	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		String line = value.toString();
		String[] elements = line.split(",");
		String[] dateElements = elements[9].split("\\W+");
		if(dateElements.length == 3) {
			String dateString = new String(dateElements[1] + "/" + dateElements[2]);
			SimpleDateFormat df = new SimpleDateFormat("MM/yyyy");
			try {
				Date date = df.parse(dateString);
				_key.set(date.getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			// add a number for counting in the reduce step
			_value.set(1);
			context.write(_key, _value);
		} else {
			context.getCounter(Accidents.POORLY_FORMATTED).increment(1);
		}
	}
}

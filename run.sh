#!/bin/bash
hadoop fs -rm -r project/run;
hadoop fs -rm -r project/moving;
hadoop fs -rm -r project/exponential;
# Usage: <input path> <output path>
hadoop jar finalProject.jar rvink.DateDriver project/accidents project/run;
# Usage: <input path> <output path> <reducer> <alpha>
hadoop jar finalProject.jar rvink.ExponentialAverageDriver project/run project/exponential exp .5;
# Usage: <input path> <output path> <reducer>
hadoop jar finalProject.jar rvink.MovingAverageDriver project/run project/moving moving;

echo "Getting the first step of the run...";
hadoop fs -get project/run;
echo "Getting the exponential values of the run...";
hadoop fs -get project/exponential;
echo "Getting the moving values of the run...";
hadoop fs -get project/moving;

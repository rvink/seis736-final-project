package rvink;

import java.io.IOException;
import java.text.SimpleDateFormat;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ExponentialAverageReducer  extends Reducer<DateTimeKey, IntWritable, Text, DoubleWritable> {

	private ExponentialMovingAverage _avg = new ExponentialMovingAverage();	
	private DoubleWritable _value = new DoubleWritable();
	private Text _key = new Text();
	
	
	
	@Override
	public void reduce(DateTimeKey key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		_avg.setAlpha(Float.parseFloat(context.getConfiguration().get("alpha")));
		for(IntWritable value : values) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yyyy");
			_key.set(simpleDateFormat.format(key.getDatetime()));
			_value.set(_avg.average(value.get()));
			context.write(_key, _value);
		}
	}
	
	class ExponentialMovingAverage {
		private double alpha;
	    private Double oldValue;
	    /*
	    public ExponentialMovingAverage() {
	        this.alpha = alpha;
	    }
	    */
	    public void setAlpha(double alpha) {
	    	this.alpha = alpha;
	    }

	    public double average(double value) {
	        if (oldValue == null) {
	            oldValue = value;
	            return value;
	        }
	        double newValue = oldValue + alpha * (value - oldValue);
	        oldValue = newValue;
	        return newValue;
	    }
	}
}

Download the zip file found at:

http://data.gov.uk/dataset/road-accidents-safety-data/resource/80b76aec-a0a1-4e14-8235-09cc6b92574a

Save the Accidents.csv on the hadoop server, move the file into $HOME/project/accidents.

Compile the java src code.

use run.sh to do the 3 map reduce algorithms in succession.

See run.sh for the implementations of the 3 algorithms.

The result sets can be found in:
exp
exp01
exp7
moving

